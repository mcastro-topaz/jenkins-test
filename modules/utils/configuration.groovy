import groovy.transform.Field

@Field final K_ENVIRONMENTS_FILE_NAME="config/environments.json"

def getEnvironmentConfiguration(environment) {
    def configuration = readJSON file: K_ENVIRONMENTS_FILE_NAME
    def environmentParams = configuration.environments."${environment}"
    return environmentParams
}

return this