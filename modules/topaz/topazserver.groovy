import groovy.transform.Field

@Field final K_WF_STARTED_OK="WFLYSRV0025"
@Field final K_WF_STARTED_WITH_ERRORS="WFLYSRV0026"

def runTopaz(remoteTopaz, wfHomePath) {

    invokeRunTopaz(remoteTopaz, wfHomePath)
    def result = waitTopazStartup(remoteTopaz, wfHomePath)
    if(result.levantoOk) { // Si levanto Topaz, valido que no hayan fallado otros servicios
        def failedDeploys = sshCommand(remote: remoteTopaz, command: "cd $wfHomePath/standalone/deployments/ && ls | grep failed", failOnError: false)
        if(failedDeploys != "") {
            result = [levantoOk : false, 
                error: "Topaz levantó, pero se encontraron otros deploys fallidos: \n'$failedDeploys'"]
        }
    }
    return result
}

def invokeRunTopaz(remoteTopaz, wfHomePath) {
    sshCommand(remote: remoteTopaz, command: "cd $wfHomePath/bin/ && ./runinbackground.sh")
}

def waitTopazStartup(remoteTopaz, wfHomePath) {
    sshCommand(remote: remoteTopaz, command: """tail -f $wfHomePath/standalone/log/server.log | grep -qE '$K_WF_STARTED_OK|$K_WF_STARTED_WITH_ERRORS' """, failOnError: true)

    def topazFailedFileName = "$wfHomePath/standalone/deployments/topaz.ear.failed"
    def topazDeployError = sshCommand(remote: remoteTopaz, command: "[ -r $topazFailedFileName ] && cat $topazFailedFileName ", failOnError: false)

    if(!topazDeployError.trim().equals("")) {
        return [levantoOk : false, 
                error: "Error en topaz.failed: $topazDeployError"]

    } else {
        return [levantoOk : true]
    }
}


def killTopazProcess(remoteTopaz, wfFolderName) {
    sshCommand(remote: remoteTopaz, command: """pkill -f "$wfFolderName" --signal 9""")
}

return this