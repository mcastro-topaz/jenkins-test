/*
import groovy.transform.Field

@Field final K_VPC_API_ENDPOINT   = "https://us-south.iaas.cloud.ibm.com"

@Field final K_VPC_TOKEN_ENDPOINT = "https://iam.cloud.ibm.com/identity/token"
@Field final K_VPC_API_VERSION    = "2020-07-28"
@Field final K_VPC_API_KEY        = "cEC1c2UmgvKgA6Nu-jF8UQXwKgcC7_5RFwqt_mIDICT3"

@Field def iam_token
*/

def serverActionTEST () {    
    def httpResponse = httpRequest acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'GET', url: "https://e3a00cee-f097-45a1-a303-2a761173ae2d.mock.pstmn.io/prueba500"    
    if(httpResponse.status==500){
        throw new Exception("Error 500" + httpResponse.content)
    }

}


/*
def initAccessToken () {
    def httpRequestBody = "grant_type=urn:ibm:params:oauth:grant-type:apikey&apikey=${K_VPC_API_KEY}"
    def httpResponse = httpRequest acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_FORM', httpMode: 'POST', requestBody: httpRequestBody, url: K_VPC_TOKEN_ENDPOINT

    def httpResponseContent  = readJSON text: httpResponse.content

    echo "Obtención del IAM Token - httpStatus: ${httpResponse.status} - content.access_token ${httpResponseContent.access_token}"

    iam_token = httpResponseContent.access_token    
}

def serverAction (instanceId, action) {
    
    def httpRequestBody = """{"type": "${action}"}"""
    def httpResponse = httpRequest  customHeaders: [[name: 'Authorization', value: "Bearer ${iam_token}"]], acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'POST', requestBody: httpRequestBody, url: "${K_VPC_API_ENDPOINT}/v1/instances/${instanceId}/actions?version=${K_VPC_API_VERSION}&generation=2"
    def httpResponseContent  = readJSON text: httpResponse.content
    echo "httpStatus: ${httpResponse.status} - httpResponse.content: ${httpResponse.content}"    
}

def getInstanceState (instanceId) {
    def httpResponse = httpRequest  customHeaders: [[name: 'Authorization', value: "Bearer ${iam_token}"]], acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'GET', url: "${K_VPC_API_ENDPOINT}/v1/instances/${instanceId}?version=${K_VPC_API_VERSION}&generation=2"
    def httpResponseContent  = readJSON text: httpResponse.content
    echo "httpStatus: ${httpResponse.status} - httpResponse.content.status: ${httpResponseContent.status}"

    return httpResponseContent.status
}

def isInstanceRunning(instanceId) {
    def instanceState = getInstanceState(instanceId)
    return instanceState.equals("running")
}

def waitInstanceRunning(instanceId, timeout) {
    // Espero que los servidores levanten
    def tiempoEspera = 5
    def cantidadIntentos = (timeout / tiempoEspera) // Para que el timeout sea a los 5 minutos (1 min = 12 esperas de 5 seg)
    def timedOut = true
    for(int i = 0; i < cantidadIntentos; i++) {
        // Consulto el estado del servidor
        def isServerRunning = ibmvpc.isInstanceRunning(instanceId)

        if(isServerRunning) {
            timedOut = false
            break
        } else {
            sleep(tiempoEspera)            
        }
    } 
    if(timedOut) {
        throw new Exception("Se superó el tiempo máximo de espera definido para el inicio del servidor. ID servidor: '%s'.", instanceId)
    }
}
*/
return this